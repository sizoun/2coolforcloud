# 2cool4cloud

Un script très artisanal qui transforme les contours d'une typo en plusieurs arcs avec [svgpathtools](https://pypi.org/project/svgpathtools/)

![2cool4cloud](2cool4cloud.jpg )

## Dépendances

Python 3, svgpathtools, lxml

## Fonctionnement

- Dans le dossier où se trouve le script, glisser la typo à transformer.
- Dans le terminal, `python3 2cool4cloud.py nomDeLaTypo.format` (par exemple, `python3 2cool4cloud.py garamond.otf`)

## Paramètres à expérimenter dans le script

- *coeff* : 1 = beaucoup d'arcs, 10 = peu d'arcs
- *stroke* : épaisseur du contour
- *rot* : rotation des arcs
- *rayon* : rayon de l'arc (sous la forme `x + yj`)


