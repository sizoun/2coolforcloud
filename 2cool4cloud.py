#!/usr/bin/env python3

import fontforge, os, re, glob, shutil, sys
from svgpathtools import *
import lxml.etree as ET

fontname = sys.argv[1] # fonte à transformer

coeff = 1 # largeur des arcs

rot = 0 # rotation des arcs

stroke = 20 # épaisseur du trait

rayon = 1 + 1j # rayon de l'arc

try: # création des dossiers contenant les SVG
    os.mkdir("svg")
    os.mkdir("svg2")
except:
    pass

font = fontforge.open(fontname)
font_ascent, font_descent = font.ascent, font.descent

for gly in font.glyphs():
    gly.export("svg/" + gly.glyphname + ".svg")

def cloud(svg):  # fonction pour transformer les contours

    paths, attributes = svg2paths(svg) # on récupère les paths de chaque svg
    xmin, xmax, ymin, ymax = paths[0].bbox()
    width, height = xmax - xmin, ymax - ymin
    tree = ET.parse(svg)
    root = tree.getroot()
    p = root.find(".//{http://www.w3.org/2000/svg}path")
    d_string = p.get("d")

    all_paths = d_string.replace("zM", "z/M").split("/") # on récupère les segments de chaque path

    n = font_ascent / 10 * (coeff / 10 + 1)

    total_path = []

    for p in all_paths:
        all_paths = []
        points = []
        path = parse_path(p)
        n_path = int(path.length() / n)

        if n_path < 4:
            n_path = 4

        for i in range(n_path):
            points.append(path.point(i / (n_path - 1))) # on divise le segment et récupère les coordonnées des points

        for k in range(len(points) - 1): # création des arcs à partir des points
            try:
                total_path.append(
                    Arc(
                        start=points[k],
                        radius=rayon,
                        rotation=rot,
                        large_arc=0,
                        sweep=1,
                        end=points[k + 1],
                    )
                )
            except:
                pass

    attr = {
        "fill": "none",
        "stroke-width": stroke,
        "stroke": "black",
        "stroke-linecap": "round",
    }

    attrs = [attr] * len(total_path)

    wsvg(
        total_path,
        attributes=attrs,
        svg_attributes={"width": width, "height": height},
        filename=svg.replace("svg/", "svg2/"),
    )

def makeFont(fontname):
    svgDir = glob.glob("svg2/*.svg")
    font = fontforge.font(fontname + ".sfd")
    font.descent = font_descent
    font.ascent = font_ascent
    font.fontname = fontname

    for glyph in svgDir:
        with open(glyph, "rt") as f:
            treeLet = ET.parse(f)
            rootLet = treeLet.getroot()
            chasse = int(float(rootLet.get("width")))

            letter = glyph.split("/")[-1].replace(".svg", "")
            try:
                char = font.createMappedChar(letter)
                char.width = chasse
                char.importOutlines(glyph, scale=True)
                char.left_side_bearing = 5
                char.right_side_bearing = 5
            except:
                pass

    font.generate(fontname + ".otf")

    try: # suppression des dossiers contenant les SVG
        shutil.rmtree("svg")
        shutil.rmtree("svg2")
    except:
        pass

for lettre in glob.glob("svg/*.svg"):

    try:
        cloud(lettre)
    except:
        pass


makeFont(fontname.split(".")[0] + "NUAGE")

